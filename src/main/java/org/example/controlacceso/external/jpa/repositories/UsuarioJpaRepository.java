package org.example.controlacceso.external.jpa.repositories;

import org.example.controlacceso.external.jpa.model.UsuarioJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioJpaRepository extends JpaRepository<UsuarioJpa, Integer> {
}
