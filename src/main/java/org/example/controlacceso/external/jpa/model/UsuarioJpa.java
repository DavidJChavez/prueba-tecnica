package org.example.controlacceso.external.jpa.model;

import jakarta.persistence.*;
import lombok.*;
import org.example.controlacceso.core.entity.Usuario;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "tu01_usuario")
public class UsuarioJpa {
    @Id
    @SequenceGenerator(name = "tu01_usuario_id_usuario_seq", sequenceName = "tu01_usuario_id_usuario_seq", allocationSize = 1)
    @GeneratedValue(generator = "tu01_usuario_id_usuario_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Integer id;
    @Column(name = "tx_nombre")
    private String nombre;
    @Column(name = "tx_correo")
    private String correo;
    @Column(name = "tx_password")
    private String password;

    public static UsuarioJpa fromEntity(Usuario entity) {
        return UsuarioJpa.builder()
                .id(entity.getId())
                .nombre(entity.getNombre())
                .correo(entity.getCorreo())
                .password(entity.getPassword())
                .build();
    }

    public Usuario toEntity() {
        return Usuario.builder()
                .id(id)
                .nombre(nombre)
                .correo(correo)
                .password(password)
                .build();
    }
}
