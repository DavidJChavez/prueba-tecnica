package org.example.controlacceso.external.jpa.dao;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.example.controlacceso.core.business.output.UsuarioRepository;
import org.example.controlacceso.core.entity.Usuario;
import org.example.controlacceso.external.jpa.model.UsuarioJpa;
import org.example.controlacceso.external.jpa.repositories.UsuarioJpaRepository;

import java.util.Optional;

@ApplicationScoped
public class UsuarioDao implements UsuarioRepository {
    @Inject
    UsuarioJpaRepository usuarioJpaRepository;

    @Override
    public Optional<Usuario> findById(Integer id) {
        return usuarioJpaRepository.findById(id).map(UsuarioJpa::toEntity);
    }

    @Override
    public Usuario save(Usuario newUsuario) {
        return null;
    }

    @Override
    public void update(Usuario usuario) {

    }

    @Override
    public void delete(Integer id) {

    }
}
