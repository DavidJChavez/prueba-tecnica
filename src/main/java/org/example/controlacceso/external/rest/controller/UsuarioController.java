package org.example.controlacceso.external.rest.controller;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.example.controlacceso.core.business.input.UsuarioService;
import org.example.controlacceso.external.rest.dto.GetUsuarioDTO;
import org.example.util.error.ErrorMapper;
import org.example.util.error.ErrorResponseDTO;

@Path("/usuario")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Usuario")
public class UsuarioController {
    @Inject
    UsuarioService usuarioService;

    @GET
    @Path("/{idUsuario}")
    @APIResponse(responseCode = "200", description = "Respuesta exitosa", content = @Content(schema = @Schema(implementation = GetUsuarioDTO.class)))
    @APIResponse(responseCode = "400", description = "Respuesta exitosa", content = @Content(schema = @Schema(implementation = ErrorResponseDTO.class)))
    @Operation(operationId = "getUsuarioById", description = "Obtiene un usuario por su id")
    public Response getById(@PathParam("idUsuario") Integer idUsuario) {
        return usuarioService.getById(idUsuario)
                .map(GetUsuarioDTO::fromEntity)
                .map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder)
                .build();
    }
}
