package org.example.controlacceso.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.example.controlacceso.core.entity.Usuario;

@AllArgsConstructor
@Builder
@Getter
@Schema(name = "Usuario", description = "DTO para obtener información del usuario")
public class GetUsuarioDTO {
    @JsonProperty
    @Schema(readOnly = true, description = "Identificador del usuario")
    private Integer idUsuario;
    @JsonProperty
    @Schema(readOnly = true, description = "Nombre del usuario")
    private String nombre;
    @JsonProperty
    @Schema(readOnly = true, description = "Correo del usuario")
    private String correo;

    public static GetUsuarioDTO fromEntity(Usuario entity) {
        return GetUsuarioDTO.builder()
                .idUsuario(entity.getId())
                .nombre(entity.getNombre())
                .correo(entity.getCorreo())
                .build();
    }
}
