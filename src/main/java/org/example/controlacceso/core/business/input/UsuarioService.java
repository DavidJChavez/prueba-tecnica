package org.example.controlacceso.core.business.input;

import io.vavr.control.Either;
import org.example.controlacceso.core.entity.Usuario;
import org.example.util.error.ErrorCodesEnum;

public interface UsuarioService {
    Either<ErrorCodesEnum, Usuario> getById(Integer id);
    Either<ErrorCodesEnum, Boolean> create(Usuario newUsuario);
    Either<ErrorCodesEnum, Boolean> update(Usuario newUsuario);
    Either<ErrorCodesEnum, Boolean> deleteById(Integer id);
}
