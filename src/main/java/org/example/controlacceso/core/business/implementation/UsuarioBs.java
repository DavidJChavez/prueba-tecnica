package org.example.controlacceso.core.business.implementation;

import io.vavr.control.Either;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.example.controlacceso.core.business.input.UsuarioService;
import org.example.controlacceso.core.business.output.UsuarioRepository;
import org.example.controlacceso.core.entity.Usuario;
import org.example.util.error.ErrorCodesEnum;

@Slf4j
@ApplicationScoped
public class UsuarioBs implements UsuarioService {
    @Inject
    UsuarioRepository usuarioRepository;

    @Override
    public Either<ErrorCodesEnum, Usuario> getById(Integer id) {
        Either<ErrorCodesEnum, Usuario> result;
        var optionalUsuario = usuarioRepository.findById(id);
        result = optionalUsuario.
                <Either<ErrorCodesEnum, Usuario>>map(Either::right)
                .orElseGet(() -> Either.left(ErrorCodesEnum.NOT_FOUND));
        return result;
    }

    @Override
    public Either<ErrorCodesEnum, Boolean> create(Usuario newUsuario) {
        return null;
    }

    @Override
    public Either<ErrorCodesEnum, Boolean> update(Usuario newUsuario) {
        return null;
    }

    @Override
    public Either<ErrorCodesEnum, Boolean> deleteById(Integer id) {
        return null;
    }
}
