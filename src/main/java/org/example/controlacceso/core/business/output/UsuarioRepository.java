package org.example.controlacceso.core.business.output;

import org.example.controlacceso.core.entity.Usuario;

import java.util.Optional;

public interface UsuarioRepository {
    Optional<Usuario> findById(Integer id);
    Usuario save(Usuario newUsuario);
    void update(Usuario usuario);
    void delete(Integer id);
}
