package org.example.controlacceso.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class Usuario {
    private Integer id;
    private String nombre;
    private String correo;
    private String password;
}
