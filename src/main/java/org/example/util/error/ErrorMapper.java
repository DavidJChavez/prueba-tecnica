package org.example.util.error;

import jakarta.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ErrorMapper {
    public static Response.ResponseBuilder errorCodeToResponseBuilder(ErrorCode code) {
        return Response.status(Response.Status.BAD_REQUEST).entity(errorCodeToResponseBuilder(code));
    }
}
