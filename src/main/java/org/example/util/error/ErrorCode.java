package org.example.util.error;

public interface ErrorCode {
    String getName();
    String getDetail();
}
