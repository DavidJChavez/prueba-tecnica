package org.example.util.error;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
@Builder
@AllArgsConstructor
@Schema(name = "ErrorResponse", description = "Dto para errores")
public class ErrorResponseDTO {
    @JsonProperty
    @Schema(description = "Código del error")
    private String code;
    @JsonProperty
    @Schema(description = "MensajeNacional del error")
    private String message;

    public ErrorResponseDTO() {
        code = ErrorCodesEnum.ERROR_DESCONOCIDO.name();
        message = ErrorCodesEnum.ERROR_DESCONOCIDO.getDetail();
    }
}
