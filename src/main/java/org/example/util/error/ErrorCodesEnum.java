package org.example.util.error;

public enum ErrorCodesEnum implements ErrorCode {
    ERROR_DESCONOCIDO("Error desconocido"),
    RN001("Campos obligatorios"),
    NOT_FOUND("Recurso no encontrado")
    ;

    private String detail;

    ErrorCodesEnum(String detail) {
        this.detail = detail;
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public String getDetail() {
        return this.detail;
    }
}
